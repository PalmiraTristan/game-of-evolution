package utils;

import scala.annotation.tailrec;

object Maths {
	// Multi-threaded matrix multiplication with parallel collections
	// WARNING This is only useful with very big data
	def multiplyPar(m1: Array[Array[Double]], m2: Array[Array[Double]]): Array[Array[Double]] = {
		val res = Array.ofDim[Double](m1.length, m2(0).length);
		val M1_COLS = m1(0).length;
		val M1_ROWS = m1.length;
		val M2_COLS = m2(0).length;
		 
		@inline
		def singleThreadedMultiplicationFAST(start_row: Int, end_row: Int) {
			var col, i = 0;
			var sum = 0.0;
			
			var row = start_row;
			while(row < end_row) {
				col = 0;
				while(col < M2_COLS) {
					i = 0; sum = 0;
					while(i < M1_COLS) {
						sum += m1(row)(i) * m2(i)(col);
						i+=1;
					}

					res(row)(col) = sum;
					col += 1;
				}
				
				row += 1;
			}
		}

		(0 until M1_ROWS).par.foreach(i => singleThreadedMultiplicationFAST(i, i+1));

		res;
	}
	
	// 3Dtensor-vector multiplication
	// WARNING This is only useful with very big data
	def multiplyParSeq(t: Array[Array[Array[Double]]], v: Array[Double]): Array[Double] = {
		val DEPTH = t.length;
		
		val res = Array.ofDim[Double](DEPTH);
		
		(0 until DEPTH).par.foreach(i => res(i) = multiplySeq(v, multiplySeq(t(i), v)));
		
		res;
	}
	
	// 3Dtensor-vector multiplication
	def multiplySeqSeq(t: Array[Array[Array[Double]]], v: Array[Double]): Array[Double] = {
		val DEPTH = t.length;
		
		val res = Array.ofDim[Double](DEPTH);
		
		(0 until DEPTH).foreach(i => res(i) = multiplySeq(v, multiplySeq(t(i), v)));
		
		res;
	}
	
	// Sequential matrix-vector multiplication
	def multiplySeq(m: Array[Array[Double]], v: Array[Double]): Array[Double] = {
		val M_COLS = m(0).length;
		val M_ROWS = m.length;
		
		val res = Array.ofDim[Double](M_ROWS);
		
		@inline
		def singleThreadedMultiplicationFAST(start_row: Int, end_row: Int) {
			var col, i = 0;
			var sum = 0.0;
			
			var row = start_row;
			while(row < end_row) {
				i = 0; sum = 0;
				while(i < M_COLS) {
					sum += m(row)(i) * v(i);
					i+=1;
				}

				res(row) = sum;
				
				row += 1;
			}
		}

		(0 until M_ROWS).foreach(i => singleThreadedMultiplicationFAST(i, i+1));

		res;
	}
	
	// matrix-vector multiplication with parallel collections
	// WARNING This is only useful with very big data
	def multiplyPar(m: Array[Array[Double]], v: Array[Double]): Array[Double] = {
		val M_COLS = m(0).length;
		val M_ROWS = m.length;
		
		val res = Array.ofDim[Double](M_ROWS);
		
		@inline
		def singleThreadedMultiplicationFAST(start_row: Int, end_row: Int) {
			var col, i = 0;
			var sum = 0.0;
			
			var row = start_row;
			while(row < end_row) {
				i = 0; sum = 0;
				while(i < M_COLS) {
					sum += m(row)(i) * v(i);
					i+=1;
				}

				res(row) = sum;
				
				row += 1;
			}
		}

		(0 until M_ROWS).par.foreach(i => singleThreadedMultiplicationFAST(i, i+1));

		res;
	}
	
	// vector-vector sequential dot product
	def multiplySeq(v1: Array[Double], v2: Array[Double]): Double = {
		var res: Double = 0;
		
		var row = 0;
		while(row < v1.length) {
			res += v1(row) * v2(row);
			
			row += 1;
		}
		
		res;
	}
	
	def add(a: (Int, Int), b: (Int, Int)): (Int, Int) = (a._1 + b._1, a._2 + b._2);
	
	def product[T](l: List[List[T]]): List[List[T]] = l match {
		case Nil => List(Nil);
		case h :: t => for(xh <- h; xt <- product(t)) yield xh :: xt;
	}
    
	def softMax(vec: Array[Double]): Array[Double] = {
		val expV = vec.map(x => math.exp(x));
		val total = expV.sum;
	
		expV.map(x => (x / total));
	}
	
	@inline
	def sigmoid(x: Double): Double = {
        return 1.0 / (1.0 + math.pow(math.E, -x));
    }
	
	def sigmoid(vec: Array[Double]): Array[Double] = {
        return vec.map(sigmoid);
    }
	
	def randomPick(probabilities: Array[Double]): Int = {
		@tailrec
		def aux(v: List[Double], pr: Double, index: Int): Int = v match {
			case h::t if (pr > h) => aux(t, (pr - h), (index + 1));
			case _ => index;
		}
		
		aux(probabilities.toList, scala.util.Random.nextDouble, 0);
	}
	
	def bool2int(b: Boolean) = if (b) 1 else 0;
}

