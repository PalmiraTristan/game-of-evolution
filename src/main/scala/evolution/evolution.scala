// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//scalac -deprecation evolution.scala animal.scala
//scala -J-Xmx2g evolution.Main --configFile config-universe.txt

//java -jar evolution.jar evolution --configFile config-universe.txt
//java -jar evolution.jar evolution --configFile config-universe.txt --logDir /mnt/entrepot/EvolutionData/35-100--100-100/

//scala -J-server -J-Xmx6g evolution.Benchmark --configFile config-benchmark.txt

// For simple remote actor in Scala: http://stackoverflow.com/questions/4340277/how-do-i-disconnect-a-scala-remote-actor -> only with a version of Scala with Akka

package evolution;

import scala.actors.Actor;
import scala.actors.Actor._;
import scala.annotation.tailrec;
import scala.collection.immutable;
import scala.collection.mutable;
import io.Source;
import scala.math.ceil;
import scala.math.exp;
import scala.math.log;
import scala.math.max;
import scala.math.min;
import scala.math.pow;
import scala.math.sqrt;
import scala.util.Try;

import animal._;

object Evolution {
	def main(args: Array[String]): Unit = {
		val randGen: scala.util.Random = scala.util.Random;
		
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
		    "mapLength",
		    "mapHeight",
		    "nbProc",
		    "nbIter",
		    "sheepDna",
		    "wolfDna",
		    "timeManager",
		    "logDir",
		    "sheepA",
		    "wolfA"
		);
		
		val arguments: mutable.Map[String, String] = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val mapLength: Int = Try(arguments("mapLength").toInt).toOption match {
		    case None => 128;
		    case Some(value) => value;
		}
		
		Console.println("mapLength = " + mapLength);
		
		val mapHeight: Int = Try(arguments("mapHeight").toInt).toOption match {
		    case None => 128;
		    case Some(value) => value;
		}
		
		Console.println("mapHeight = " + mapHeight);
		
		val nbProc: Int = Try(arguments("nbProc").toInt).toOption match {
		    case None => 4;
		    case Some(value) => value;
		}
		
		Console.println("nbProc = " + nbProc);
		
		val sheepFactory: (((Int, Int), scala.util.Random) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int), scala.util.Random) => Wolf) = Main.loadWolfFactory(arguments);
		
		// TODO So far only the universe stops after nbIter, and not the server
		// This is because the server listen to the input from the user
		val nbIter: Int = Try(arguments("nbIter").toInt).toOption match {
		    case None => -1; // ~ never
		    case Some(value) => value;
		}
		
		val timeManagerOpt: Option[TimeManager] = Try(arguments("timeManager")).toOption match {
		    case Some(str) => {
		    	val info = str.split("-", 2);
		    	
		    	info(0) match {
		    		case "ratio" => Some(new ComputationRatio(info(1).toDouble));
		    		case "fixedDuration" => Some(new FixedTurnDuration(info(1).toInt));
		    		case "none" | _ => None;
		    	}
		    }
		    
		    case _ => None;
		}
			
		val loggerOpt = Main.loadLogger(arguments);
		
		val universe = if(nbProc < 2) {
		    def processFactory(animal: Animal): AnimalSimple = {
		        new AnimalSimpleImp(animal);
		    }
		
		    // Generate the map
		    val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile[AnimalSimple](None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
			
			new UniverseSeqLogger(map, false, nbIter, false, timeManagerOpt, loggerOpt);
		}
		else {
		    def processFactory(animal: Animal): AnimalActor = {
		        new AnimalActorImp(animal);
		    }
		
		    // Generate the map
		    val map = new MapImp[AnimalActor](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile[AnimalActor](None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalActor](), processFactory, wolfFactory, sheepFactory, 0, 0);
			
			new UniverseParLogger(map, false, nbIter, false, nbProc, timeManagerOpt, loggerOpt);
		}
		
		Console.println("universe = " + universe);
		
		val server = new LocalServer(universe);
		server.start;
	}
}

object Main {
	def main(args: Array[String]): Unit = {
		Console.println("Welcome to Evolution!\n");
		
		if(args.length > 0) {
			args(0) match {
				case "benchmark" => {
					Benchmark.main(args.drop(1));	
				}
				
				case "-h" | "-help" | "--help" => {
					Console.println("benchmark | evolution");
				}
				
				case "evolution" => {
					Evolution.main(args.drop(1));
				}
				
				case _ => {
					Evolution.main(args);
				}
			}
		}
		else {
			Evolution.main(args);
		}
	}
	
	def loadLogger(arguments: mutable.Map[String, String]): Option[Logger] = {
		val modules = mutable.ListBuffer[LoggerModule]();
		
		arguments.get("PosLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				modules += new PositionModule(opts(0), opts(1).toInt);
			}
			
			case _ => ();
		}
		
		arguments.get("DemLogger") match {
			case Some(str) => {
				val opts = str.split("\t");
				modules += new DemographicModule(opts(0), opts(1).toInt);
			}
			
			case _ => ();
		}
		
		if(modules.length > 0) Some(new ModularLogger(modules.toList));
		else None;
	}
	
	def loadSheepFactory(arguments: mutable.Map[String, String]): (((Int, Int), scala.util.Random) => Sheep) = {
		val sheepDna: String = Try(arguments("sheepDna")).toOption match {
			case Some(str) => str;
			case _ => "nn--mat";
		}
	
		val sheepReference: SheepGrowUpReference = {
			val lifeExpectancy: Int = Try(arguments("sheepLE").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val energyLimit: Int = Try(arguments("sheepEL").toInt).toOption match {
				case Some(value) => value;
				case _ => 69;
			}
		
			val hungerRate: Int = Try(arguments("sheepHR").toInt).toOption match {
				case Some(value) => value;
				case _ => 4;
			}
		
			val sightRange: Int = Try(arguments("sheepSR").toInt).toOption match {
				case Some(value) => value;
				case _ => 2;
			}
		
			val maxHealthPoints: Int = Try(arguments("sheepHP").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val strength: Int = Try(arguments("sheepS").toInt).toOption match {
				case Some(value) => value;
				case _ => 35;
			}
		
			val adulthood: (Int, Int) = Try(arguments("sheepA").split("-").map(_.toInt)).toOption match {
				case Some(value) if value.length == 2 => (value(0), value(1));
				case _ => (10, 80);
			}
		
			new SheepGrowUpReference(lifeExpectancy, energyLimit, hungerRate, sightRange, maxHealthPoints, strength, adulthood);
		}
		
		val dnaGenerator = Main.parseDnaStr(sheepDna, sheepReference.perceptionVecSize, SheepBasic.numberActions, 0.1);
		
		Console.println(dnaGenerator(scala.util.Random).description);
	
		((position, randGen) => new SheepImpGrowUp(sheepReference, position, dnaGenerator(randGen), 0, 0)); // Generates babies (age = 0)
	}
	
	def loadWolfFactory(arguments: mutable.Map[String, String]): (((Int, Int), scala.util.Random) => Wolf) = {
		val wolfDna: String = Try(arguments("wolfDna")).toOption match {
			case Some(str) => str;
			case _ => "nn--mat";
		}
		
		val wolfReference: WolfGrowUpReference = {
			val lifeExpectancy: Int = Try(arguments("wolfLE").toInt).toOption match {
				case Some(value) => value;
				case _ => 200;
			}
		
			val energyLimit: Int = Try(arguments("wolfEL").toInt).toOption match {
				case Some(value) => value;
				case _ => 50;
			}
		
			val hungerRate: Int = Try(arguments("wolfHR").toInt).toOption match {
				case Some(value) => value;
				case _ => 1;
			}
		
			val sightRange: Int = Try(arguments("wolfSR").toInt).toOption match {
				case Some(value) => value;
				case _ => 2;
			}
		
			val maxHealthPoints: Int = Try(arguments("wolfHP").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val strength: Int = Try(arguments("wolfS").toInt).toOption match {
				case Some(value) => value;
				case _ => 100;
			}
		
			val adulthood: (Int, Int) = Try(arguments("wolfA").split("-").map(_.toInt)).toOption match {
				case Some(value) if value.length == 2 => (value(0), value(1));
				case _ => (20, 160);
			}
		
			new WolfGrowUpReference(lifeExpectancy, energyLimit, hungerRate, sightRange, maxHealthPoints, strength, adulthood); // Generates babies (age = 0)
		}
	
		val dnaGenerator = Main.parseDnaStr(wolfDna, wolfReference.perceptionVecSize, WolfBasic.numberActions, 0.1);
		
		Console.println(dnaGenerator(scala.util.Random).description);
		
		((position, randGen) => new WolfImpGrowUp(wolfReference, position, dnaGenerator(randGen), 0, 0));
	}
	
	def parseDnaStr(dnaStr: String, perceptionVecSize: Int, nbActions: Int, mutationFactor: Double): (scala.util.Random => Dna) = {
		@tailrec
		def analyseNN(layerStrs: List[String], inSize: Int, result: List[scala.util.Random => NNLayer], finalF: (Array[Double] => Array[Double]), finalSize: Option[Int]): (List[scala.util.Random => NNLayer], Int) = layerStrs match {
			case Nil => (result, 0); // TODO exception
			
			// Last layer
			case List(layerStr) => {
				val layerOpts = layerStr.split("-");

				val outSize = finalSize.getOrElse(layerOpts(1).toInt);
				val f = finalF;

				val newLayerGen: (scala.util.Random => NNLayer) = layerOpts(0) match {
					case "mat" => (randGen => NNLayerMatrix(randGen, outSize, inSize, f, mutationFactor));
					case "3dt" | _ => (randGen => NNLayer3DTensor(randGen, outSize, inSize, f, mutationFactor));
				}
				
				((newLayerGen :: result).reverse, outSize);
			}
		
			case layerStr :: t => {
				val layerOpts = layerStr.split("-");

				val outSize = layerOpts(1).toInt;
				val f: (Array[Double] => Array[Double]) = utils.Maths.sigmoid;

				val newLayerGen: (scala.util.Random => NNLayer) = layerOpts(0) match {
					case "mat" => (randGen => NNLayerMatrix(randGen, outSize, inSize, f, mutationFactor));
					case "3dt" | _ => (randGen => NNLayer3DTensor(randGen, outSize, inSize, f, mutationFactor));
				}
				
				analyseNN(t, outSize, newLayerGen :: result, finalF, finalSize);
			}
		}
		
		val (dnaType, dnaArg) = dnaStr.split("--", 2) match {
			case tmp => (tmp(0), tmp(1));
		}
		
		dnaType match {
			case "rnn" => {
				val stepStrs = dnaArg.split("-!-", 3);
				
				val initLayerStrs = stepStrs(0).split("--").toList;
				val (initLayerGens, outSizeInit) = analyseNN(initLayerStrs, perceptionVecSize, Nil, utils.Maths.sigmoid, None);
				
				val mainStep = stepStrs(1).split("-", 2);
				val memorySize = mainStep(0).toInt;
				val mainLayerStrs = mainStep(1).split("--").toList;
				val mainLayerGens = analyseNN(mainLayerStrs, memorySize + outSizeInit, Nil, utils.Maths.sigmoid, Some(memorySize))._1;
				
				val finalLayerStrs = stepStrs(2).split("--").toList;
				val finalLayerGens = analyseNN(finalLayerStrs, memorySize, Nil, utils.Maths.softMax, Some(nbActions))._1;
				
				(randGen: scala.util.Random) => new DnaRNNImp(initLayerGens.map(layerGen => layerGen(randGen)), mainLayerGens.map(layerGen => layerGen(randGen)), finalLayerGens.map(layerGen => layerGen(randGen)), new Array[Double](memorySize));
			}
			
			case "nn" | _ => {
				val layerStrs = dnaArg.split("--").toList;
				
				val layerGens: List[(scala.util.Random => NNLayer)] = analyseNN(layerStrs, perceptionVecSize, Nil, utils.Maths.softMax, Some(nbActions))._1;
				
				(randGen: scala.util.Random) => new DnaNNImp(layerGens.map(layerGen => layerGen(randGen)));
			}
		}
	}
    
    // Arguments parser
    def parseArguments(argumentsNames: immutable.Set[String], args: Array[String]): mutable.Map[String, String] = {
		val arguments = mutable.HashMap[String, String]();
		
		{var i = 0;
		    while(i < args.length) {
		        if(args(i).startsWith("--")) {
		            val argName = args(i).substring(2);
		            
		            if(argumentsNames(argName)) {
	                    arguments += ((argName, args(i + 1)));
	                    i += 1;
		            }
		            else {
		                Console.println("Argument \"" + args(i) + "\" unknown.");
		            }
		        }
		        else {
		            Console.println("Why \"" + args(i) + "\"?");
		        }
		        
		        i += 1;
		    }
		}
		
		arguments;
    }
}
	
case object Stop;
case object Compute;
case object StartPrinting;
case object StopPrinting;
case object SwitchPrinting;
case object TrackSheep;
case object DnaSheep;

class LocalServer(universe: Universe[_ <: AnimalProcess]) extends Actor {
    def act() = {
        universe.start;
        
        var quit = false;
        while(!quit) {
            val cmd = Console.readLine().split(' ');
            
            cmd(0) match {
                // TODO Importer un fichier d'adn
                
                case "position" => {
                	if(cmd.length == 2) {
	                	val fileName = cmd(1);
	                	//val fileName = "pos_" + (System.currentTimeMillis / 1000).toString;
	                	
	                	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	                	
	                	for(aP <- universe.map.animalProcesses.iterator if(aP.isAlive)) {
	                		val species = aP.animal match {
	                			case _: Sheep => "s";
	                			case _: Wolf => "w";
	                			case _ => "o";
	                		}
	                		
	                		val position = aP.animal.position._1 + "\t" + aP.animal.position._2;
	                		
	                		pw.write(species + "\t" + position + "\n");
	                	}
	                	
	                	pw.close();
	                }
	                else {
	                	Console.println("cmd usage: position fileName");
	                }
                }
                
                case "stop" => {
                    universe.stop = true;
                    quit = true;
                }
                
                case "sl" => {
                    universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Sheep => aP.isAlive; case _ => false;})) match {
                        case Some(aP) => {
                            aP.animal.tracker = Some(AnimalTrackerImpl);
                        }
                        
                        case _ => Console.println("sorry there is no sheep alive");
                    }
                }
                
                case "sa" => {
                    universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Sheep => true; case _ => false;})) match {
                        case Some(aP) => {
                            Console.println(aP.animal.dna);
                        }
                        
                        case _ => Console.println("sorry there is no sheep");
                    }
                }
                
                case "wl" => {
                    universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Wolf => aP.isAlive; case _ => false;})) match {
                        case Some(aP) => {
                            aP.animal.tracker = Some(AnimalTrackerImpl);
                        }
                        
                        case _ => Console.println("sorry these is no wolf alive");
                    }
                }
                
                case "wa" => {
                    universe.map.animalProcesses.find(((aP: AnimalProcess) => aP.animal match {case _: Wolf => true; case _ => false;})) match {
                        case Some(aP) => {
                            Console.println(aP.animal.dna);
                        }
                        
                        case _ => Console.println("sorry these is no wolf");
                    }
                }
                
                case "" => {
                    universe.printing = !universe.printing;
                }
                
                case cmd => Console.println("unknown command: " + cmd);
            }
        }
        
        Console.println(this + " exits");
    }
}

trait Printer {
    def print(o: Any): Unit;
    def println(o: Any): Unit;
}

object ConsolePrinter extends Printer {
    def print(o: Any): Unit = Console.print(o);
    def println(o: Any): Unit = Console.println(o);
}

class PrintServer(server: DisplayServer) extends Printer {
    def print(o: Any): Unit = server ! Print(o)
    def println(o: Any): Unit = server ! Println(o);
}

case class Print(o: Any);
case class Println(o: Any);

class DisplayServer extends Actor {
    def act() = {
        loop {
            react {
                case Print(o) => Console.println(o);
                case Println(o) => Console.println(o);
                case _ => exit();
            }
        }
    }
}

//	class Server(port: Int, symbol: Symbol, universe: Universe[_ <: AnimalProcess]) extends Actor {
//		def act() = {
//		    alive(port);
//		    register(symbol, self);
//		    
//		    System.err.println("port: " + port);
//		    System.err.println("symbol: " + symbol);
//		    
//		    universe.start;
//		    
//	    	loop {
//	            react {
//	                case DnaSheep => {
//	                    universe.map.animals.lastOption match {
//	                        case Some(s: Sheep) => {
//	                            sender ! s.dna;
//	                        }
//	                        
//	                        case _ => sender ! true;
//	                    }
//	                }
//	                
//	                case Stop => {
//	                    universe.stop = true;
//	                    
//	                    exit();
//	                }
//	                
//	                case command => {
//	                    Console.println("unknown command: " + command);
//	                }
//	            }
//	        }
//		}
//	}

trait Birth {
    def newBorn: Animal;
}

class BirthImp(val newBorn: Animal) extends Birth;

trait Death {
    def dead: Animal;
    def cause: CauseOfDeath;
}

class DeathImp(val dead: Animal, val cause: CauseOfDeath) extends Death;

trait TurnInfo {
    def time: Int;

    def births(species: AnimalSpecies): Iterable[Birth];
    def newBirth(birth: Birth): Unit;
    def nbBirths(species: AnimalSpecies): Int;
    def deaths(species: AnimalSpecies): Iterable[Death];
    def newDeath(death: Death): Unit;
    def nbDeaths(species: AnimalSpecies): Int;
    
    def nbSheeps: Int;
    def nbWolves: Int;
}

class TurnInfoImp(
	val time: Int,
	val births: mutable.Map[AnimalSpecies, List[Birth]],
	val nbBirths: mutable.Map[AnimalSpecies, Int],
	val deaths: mutable.Map[AnimalSpecies, List[Death]],
	val nbDeaths: mutable.Map[AnimalSpecies, Int],
	val nbSheeps: Int,
	val nbWolves: Int
) extends TurnInfo {
    def this(time: Int, nbSheeps: Int, nbWolves: Int) = this(time, new mutable.HashMap[AnimalSpecies, List[Birth]](), new mutable.HashMap[AnimalSpecies, Int](), new mutable.HashMap[AnimalSpecies, List[Death]](), new mutable.HashMap[AnimalSpecies, Int](), nbSheeps, nbWolves);
    
    def births(species: AnimalSpecies): Iterable[Birth] = births.getOrElse(species, Nil);
    def nbBirths(species: AnimalSpecies): Int = nbBirths.getOrElse(species, 0);
    def deaths(species: AnimalSpecies): Iterable[Death] = deaths.getOrElse(species, Nil);
    def nbDeaths(species: AnimalSpecies): Int = nbDeaths.getOrElse(species, 0);
    
    def newBirth(birth: Birth): Unit = {
        births(Species(birth.newBorn)) = birth :: births.getOrElse(Species(birth.newBorn), Nil);
    }
    
    def newDeath(death: Death): Unit = {
        deaths(Species(death.dead)) = death :: deaths.getOrElse(Species(death.dead), Nil);
    }
}

trait TimeManager {
	def sleep(computationDuration: Long): Unit;
}

class FixedTurnDuration(val turnDuration: Int) extends TimeManager {
	def sleep(computationDuration: Long): Unit = {
		val sleepDuration = (turnDuration - computationDuration);
		
		if(sleepDuration > 0) {
			Thread.sleep(sleepDuration);
		}
	}
}

class ComputationRatio(val ratio: Double) extends TimeManager {
	def sleep(computationDuration: Long): Unit = {
		Thread.sleep((computationDuration * ratio).toLong);
	}
}

trait Logger {
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit; // Called at the end of each turn
	def stop: Unit; // Called at the end of the universe
}

class ModularLogger(val modules: Iterable[LoggerModule]) extends Logger {
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		for(module <- modules) module(time, universe);
	}
	
	def stop: Unit = {
		for(module <- modules) module.stop;
	}
}

trait LoggerModule {
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit; // Called at the end of each turn
	def stop: Unit = (); // Called at the end of the universe
}

// This module logs regularly the life expectancy of the sheeps and wolves
class DemographicModule(fileName: String, val nbSteps: Int) extends LoggerModule {
	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	pw.write("#time\tnbSheeps\tnbWolves\tsheepLifeExpectancy\twolfLifeExpectancy\n");
	pw.flush;
	
	val turnInfos = new Array[TurnInfo](nbSteps);
	var i = 0;
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		turnInfos(i) = universe.currentTurnInfo;
		i += 1;
		
		if(i == nbSteps) {
			def auxLifeExpectancy(turnInfo: TurnInfo, species: AnimalSpecies, beg: (Int, Int)): (Int, Int) = turnInfo.deaths(species).foldLeft(beg)((acc: (Int, Int), x: Death) => (acc._1 + x.dead.age, acc._2 + 1));
			
			def lifeExpectancy(species: AnimalSpecies): Double = turnInfos.foldLeft((0, 0))((acc: (Int, Int), turnInfo: TurnInfo) => auxLifeExpectancy(turnInfo, species, acc)) match {
                case (_, 0) => 0.0;
                case (sum, count) => sum / count.toDouble; 
            }
            
            val sheepLifeExpectancy = lifeExpectancy(SheepType);
            val wolfLifeExpectancy = lifeExpectancy(WolfType);
            
		    val nbSheeps = turnInfos.foldLeft((0, 0))((acc: (Int, Int), turnInfo: TurnInfo) => (acc._1 + turnInfo.nbSheeps, 1 + acc._2)) match {
                case (_, 0) => 0.0;
                case (sum, count) => sum / count.toDouble; 
            }

		    val nbWolves = turnInfos.foldLeft((0, 0))((acc: (Int, Int), turnInfo: TurnInfo) => (acc._1 + turnInfo.nbWolves, 1 + acc._2)) match {
                case (_, 0) => 0.0;
                case (sum, count) => sum / count.toDouble; 
            }
            
			pw.write(time + "\t" + nbSheeps + "\t" + nbWolves + "\t" + sheepLifeExpectancy + "\t" + wolfLifeExpectancy + "\n");
			
			pw.flush;
			i = 0;
		}
	}
	
	override def stop: Unit = {
		pw.close;
	}
}

// This module logs logarithmically the positions of each animals
class PositionModule(val logPath: String, val exponent: Int = 10) extends LoggerModule {
	var i = 0;
	var k = 1;
	
	def apply(time: Int, universe: Universe[_ <: AnimalProcess]): Unit = {
		if(time % k == 0) {
			i += 1;
			
			if(i == exponent) {
				i = 1;
				k *= exponent;
			}
			
			(new java.io.File(logPath + "/" + time.toString)).mkdir;
	    	val fileName = logPath + "/" + time.toString + "/" + "positions.txt";
	    	
	    	val pw = new java.io.PrintWriter(new java.io.File(fileName));
	    	
	    	for(aP <- universe.map.animalProcesses.iterator) {
	    		val species = aP.animal match {
	    			case _: Sheep => "s";
	    			case _: Wolf => "w";
	    			case _ => "o";
	    		}
	    		
	    		val position = aP.animal.position._1 + "\t" + aP.animal.position._2;
	    		
	    		pw.write(species + "\t" + position + "\n");
	    	}
	    	
	    	pw.close();
		}
	}
}

trait Universe[T <: AnimalProcess] extends Actor {
    var currentTurnInfo: TurnInfo = null;
    val runtime = Runtime.getRuntime;
    
    def map: Map[T];
    var stop: Boolean;
    val nbIter: Int; // -1 ~ never
    var printing: Boolean;
    def timeManagerOpt: Option[TimeManager];
    
    def birth(animalProcess: T): Unit = {
        currentTurnInfo.newBirth(new BirthImp(animalProcess.animal));
    }
    
    def death(animalProcess: T, cause: CauseOfDeath): Unit = {
        currentTurnInfo.newDeath(new DeathImp(animalProcess.animal, cause));
    }
    
    def actionLoop(time: Int): Unit;
    
    def act = {
        var phase = 0;

        val infoTimer = 1000; // Every 1000 ms
        var timer: Long = 0;
        var printInfo = true;
		
		// TODO these two should be parameters
        val maxGenerateWolf = 200;
        val maxGenerateSheep = 200;

        var time = 0;
    
        while(!stop && (nbIter != time)) {
        	executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep) match {
        		case (nextPhase, nextTimer, nextPrintInfo) => {
        			phase = nextPhase;
        			timer = nextTimer;
        			printInfo = nextPrintInfo;
        		}
        	}
        	
        	time += 1;
        }
        
        Console.println(this + " exits");
    }
    
    def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
    	val timestamp0 = System.currentTimeMillis;
    	       
    	val (nbWolves, nbSheeps) = map.demographics;
        currentTurnInfo = new TurnInfoImp(time, nbSheeps, nbWolves);
			
        if(printing && printInfo) {
            System.err.print("Turn " + time + " - ");
            System.err.print("wolves: " + nbWolves + "; sheeps: " + nbSheeps + " - ");
        }
    	
    	// Demographic control
        if(phase == 0) {
	        if(nbSheeps < maxGenerateSheep) map.generateSheep(this);
            if(nbWolves < maxGenerateWolf) map.generateWolf(this);
        }
        
        // Animals' decision taking and acting
        actionLoop(time);
        
        // Various minor computations
        val nextPhase = if(phase == 0 && nbWolves > 2 * maxGenerateWolf && nbSheeps > 2 * maxGenerateSheep) 1;
        else {
	        if(phase == 1 && (nbWolves == 0 || nbSheeps == 0)) {
	        	stop = true;
	        	
	        	Console.println("wolves: " + nbWolves + "; sheeps: " + nbSheeps + "!!");
	        }
	        
	        phase;
        }
        
        if(printing && printInfo) {
            val sheepLifeExpectancy = currentTurnInfo.deaths(SheepType).foldLeft((0, 0))((acc: (Int, Int), x: Death) => (acc._1 + x.dead.age, acc._2 + 1)) match {
                case (_, 0) => 0;
                case (sum, count) => sum / count.toDouble; 
            }
            
            val wolfLifeExpectancy = currentTurnInfo.deaths(WolfType).foldLeft((0, 0))((acc: (Int, Int), x: Death) => (acc._1 + x.dead.age, acc._2 + 1)) match {
                case (_, 0) => 0;
                case (sum, count) => sum / count.toDouble; 
            }
            
        
            System.err.print("life exp: " + "%.1f".format(wolfLifeExpectancy) + "; " + "%.1f".format(sheepLifeExpectancy) + " - ");
            System.err.print((runtime.totalMemory - runtime.freeMemory) / (1024 * 1024) + " Mo - ");
        }
        
        val timestamp1 = System.currentTimeMillis; // Time after main computation
        
		val computationDuration = (timestamp1 - timestamp0);
		
        if(printing && printInfo) {
            System.err.print(computationDuration + " ms \n");
        }
        
        timeManagerOpt match {
        	case Some(timeManager: TimeManager) => timeManager.sleep(computationDuration);
    		case None => ();
        }
        
        val (nextTimer, nextPrintInfo) = if(timestamp1 - timer > infoTimer) {
        	(timestamp1, true);
        }
        else (timer, false);
        
        (nextPhase, nextTimer, nextPrintInfo);
    }
}

case class ChooseAction(val time: Int, val map: Map[AnimalActor]);
case class Choice(val animalActor: AnimalActor, val action: Action);

class UniversePar(
	val map: Map[AnimalActor],
	var stop: Boolean,
	val nbIter: Int,
	var printing: Boolean,
	val nbProc: Int,
	val timeManagerOpt: Option[TimeManager]
) extends Universe[AnimalActor] {
    override def birth(animalActor: AnimalActor): Unit = {
        super.birth(animalActor);
        
        animalActor.start;
    }
    
    override def death(animalActor: AnimalActor, cause: CauseOfDeath): Unit = {
        super.death(animalActor, cause);
        
		animalActor ! Stop;
    }

    def actionLoop(time: Int): Unit = {
        val animalActors = map.animalProcesses.iterator;
        
        var i = 0;
        
        while(i < nbProc && animalActors.hasNext) {
            animalActors.next ! ChooseAction(time, map);
            
            i += 1;
        }
        
        while(animalActors.hasNext) {
            receive {
                case Choice(animalActor, action) => {
                    (animalActors find (_.isAlive)) match {
                        case Some(aP) => aP ! ChooseAction(time, map);
                        case None => ();
                    }
                    
                    if(animalActor.isAlive) { // It's unlikely but the animal might have died
                        animalActor.performAction(action, time, map, this);
                        animalActor.getOlder(map, this)
                    }
                }
                
                //case _ => ();
            }
        }
       
        for(j <- 1 to i) {
            receive {
                case Choice(animalActor, action) => {
                    if(animalActor.isAlive) { // It's unlikely but the animal might have died
                        animalActor.performAction(action, time, map, this);
                        animalActor.getOlder(map, this);
                    }
                }
                
                //case _ => ();
            }
        }
    }
    
    override def act(): Unit = {
        super.act();
        
        for(animalActor <- map.animalProcesses) {
            animalActor ! Stop;
        }
    }
}

class UniverseParLogger(
	map: Map[AnimalActor],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	nbProc: Int,
	timeManagerOpt: Option[TimeManager],
	val loggerOpt: Option[Logger]
) extends UniversePar(map, stop0, nbIter, printing0, nbProc, timeManagerOpt) {
	override def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
		val returnValue = super.executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep);
		
		loggerOpt match {
			case Some(logger) => logger(time, this);
			case _ => ();
		}
		
		returnValue;
	}
    
    override def act(): Unit = {
		val returnValue = super.act();
		
		loggerOpt match {
			case Some(logger) => logger.stop;
			case _ => ();
		}
		
		returnValue;
    }
}

class UniverseParBenchmark(
	map: Map[AnimalActor],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	nbProc: Int,
	val warmUp: Int
) extends UniversePar(map, stop0, nbIter, printing0, nbProc, None) {
    override def birth(animalActor: AnimalActor): Unit = {
        animalActor.start;
    }
    
    override def death(animalActor: AnimalActor, cause: CauseOfDeath): Unit = {
		animalActor ! Stop;
    }
    
    override def actionLoop(time: Int): Unit = {
        val animalActors = map.animalProcesses.iterator;
        
        var i = 0;
        
        while(i < nbProc && animalActors.hasNext) {
            animalActors.next ! ChooseAction(time, map);
            
            i += 1;
        }
        
        while(animalActors.hasNext) {
            receive {
                case Choice(animalActor, action) => {
                    (animalActors find (_.isAlive)) match {
                        case Some(aP) => aP ! ChooseAction(time, map);
                        case None => ();
                    }
                    
                    // Actions are not executed
                }
                
                //case _ => ();
            }
        }
        
        for(j <- 1 to i) {
            receive {
                case Choice(animalActor, action) => {
                    // Actions are not executed
                }
                
                //case _ => ();
            }
        }
    }
    
    override def act(): Unit = {
        Console.print("warm up phase");
        
        for(time <- 0 until warmUp) {
            actionLoop(time);
        }
        
        Console.println(" - complete");
        
        val beginTime = System.currentTimeMillis;
        
        for(time <- 0 until nbIter) {
            //Console.println("iter " + time);
            actionLoop(time);
        }
        
        val endTime = System.currentTimeMillis;
        
        Console.println((endTime - beginTime) + " ms");
        
        for(animalActor <- map.animalProcesses) {
            animalActor ! Stop;
        }
    }
}

class UniverseSeq(
	val map: Map[AnimalSimple],
	var stop: Boolean,
	val nbIter: Int,
	var printing: Boolean,
	val timeManagerOpt: Option[TimeManager]
) extends Universe[AnimalSimple] {
    def actionLoop(time: Int): Unit = {
        for(animalSimple <- map.animalProcesses if animalSimple.isAlive) {
            animalSimple.act(time, map, this);

            animalSimple.getOlder(map, this);
        }
    }
}

class UniverseSeqLogger(
	map: Map[AnimalSimple],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	timeManagerOpt: Option[TimeManager],
	val loggerOpt: Option[Logger]
) extends UniverseSeq(map, stop0, nbIter, printing0, timeManagerOpt) {
	override def executeTurn(time: Int, phase: Int, timer: Long, printInfo: Boolean, infoTimer: Int, maxGenerateWolf: Int, maxGenerateSheep: Int): (Int, Long, Boolean) = {
		val returnValue = super.executeTurn(time, phase, timer, printInfo, infoTimer, maxGenerateWolf, maxGenerateSheep);
		
		loggerOpt match {
			case Some(logger) => logger(time, this);
			case _ => ();
		}
		
		returnValue;
	}
    
    override def act(): Unit = {
		val returnValue = super.act();
		
		loggerOpt match {
			case Some(logger) => logger.stop;
			case _ => ();
		}
		
		returnValue;
    }
}

class UniverseSeqBenchmark(
	map: Map[AnimalSimple],
	stop0: Boolean,
	nbIter: Int,
	printing0: Boolean,
	val warmUp: Int
) extends UniverseSeq(map, stop0, nbIter, printing0, None) {
    override def birth(animalSimple: AnimalSimple): Unit = ();
    
    override def death(animalSimple: AnimalSimple, cause: CauseOfDeath): Unit = ();
    
    override def actionLoop(time: Int): Unit = {
        for(animalSimple <- map.animalProcesses if animalSimple.isAlive) {
            animalSimple.animal.chooseAction(time, map);
        }
    }
    
    override def act(): Unit = {
        Console.print("warm up phase");
        
        for(time <- 0 until warmUp) {
            actionLoop(time);
        }
        
        Console.println(" - complete");
        
        val beginTime = System.currentTimeMillis;
        
        for(time <- 0 until nbIter) {
            //Console.println("iter " + time);
            actionLoop(time);
        }
        
        val endTime = System.currentTimeMillis;
        
        Console.println((endTime - beginTime) + " ms");
    }
}

object Grass {
	val maxHeight = 20;
	val growthRate = 1;
}

class Grass(val height: Int, val maxHeight: Int, val time: Int) {
	def update(newTime: Int, change: Int): Grass = {
		new Grass(
			min(maxHeight, max(0, (height + Grass.growthRate * (newTime - time) + change))), 
			maxHeight, 
			newTime
		);
	}
}

class Tile[T <: AnimalProcess](var animalProcess: Option[T], var grass: Grass) {
	def updateGrass(time: Int): Int = updateGrass(time, 0);
	
	def updateGrass(time: Int, change: Int): Int = {
		grass = grass.update(time, change);
		
		grass.height;
	}
}

trait Map[T <: AnimalProcess] {
	def length: Int;
	def height: Int;
	def animalProcesses: mutable.Set[T];
	
	def processFactory: (Animal => T);
	
	def tile(pos: (Int, Int)): Option[Tile[T]];
	def addPosDir(a: (Int, Int), b: (Int, Int)): (Int, Int);
	def neighbourhood(pos: (Int, Int), dst: Int): List[((Int, Int), Tile[T])];
	def print: Unit;
	
	def demographics: (Int, Int) = {
		@tailrec
		def aux(iter: Iterator[T], w: Int, s: Int): (Int, Int) = iter.hasNext match {
			case false => (w, s);
			case true => iter.next.animal match {
				case _: Wolf => aux(iter, (w + 1), s);
				case _: Sheep => aux(iter, w, (s + 1));
			}
		}
		
		aux(animalProcesses.iterator, 0, 0);
	}
	
	def birth(animal: Animal, universe: Universe[T]): Unit = {
	    val animalProcess = processFactory(animal);
	    
		tile(animal.position).get.animalProcess = Some(animalProcess);
		animalProcesses += animalProcess;
		
		universe.birth(animalProcess);
	}
	
	def death(animalProcess: T, cause: CauseOfDeath, universe: Universe[T]): Unit = {
		tile(animalProcess.animal.position).get.animalProcess = None;
		animalProcesses -= animalProcess;
		
		universe.death(animalProcess, cause);
	}
	
	def generateWolf(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean;
	
	def generateSheep(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean;
}

class MapImp[T <: AnimalProcess](
    val length: Int,
    val height: Int,
    val tiles: Array[Array[Tile[T]]],
    val animalProcesses: mutable.Set[T],
    val processFactory: (Animal => T),
    val wolfFactory: (((Int, Int), scala.util.Random) => Wolf),
    val sheepFactory: (((Int, Int), scala.util.Random) => Sheep),
    var nbWolves: Int,
    var nbSheeps: Int,
    var goodWolf: Option[(Int, Wolf)] = None,
    var goodSheep: Option[(Int, Sheep)] = None
) extends Map[T] {
	override def demographics: (Int, Int) = (nbWolves, nbSheeps);
	
	override def birth(animal: Animal, universe: Universe[T]): Unit = {
		super.birth(animal, universe);
		
		animal match {
			case _: Wolf => nbWolves += 1;
			case _: Sheep => nbSheeps += 1;
		}
	}
	
	override def death(animalProcess: T, cause: CauseOfDeath, universe: Universe[T]): Unit = {
		super.death(animalProcess, cause, universe);
		
		animalProcess.animal match {
			case w: Wolf => {
			    nbWolves -= 1;
			    
	            //val score = (ceil(100 * (1 - exp(- w.ageLineage / 200.0))) * w.age).toInt;
	            val score = ((1 - exp(- 1 - w.generation)) * (if(w.generation > 0) 2 else 1) * w.age * (if(cause == Starvation) 1 else 2)).toInt;
			    
			    if(goodWolf.isEmpty || score > goodWolf.get._1) {
			        goodWolf = Some((score, w));
			        //System.err.println("best wolf score: " + score + " (" + w.age + ", " + w.ageLineage + ")");
			    }
			}
			case s: Sheep => {
			    nbSheeps -= 1;
			    
	            //val score = (ceil(100 * (1 - exp(- s.ageLineage / 100.0))) * s.age).toInt;
	            //val score = s.age * s.generation;
	            val score = ((1 - exp(- 1 - s.generation)) * (if(s.generation > 0) 2 else 1) * s.age * (if(cause == Starvation) 1 else 2)).toInt;
			    
			    if(goodSheep.isEmpty || score > goodSheep.get._1) {
			        goodSheep = Some((score, s));
			        //System.err.println("best sheep score: " + score + " (" + s.age + ", " + s.ageLineage + ")");
			    }
			    else if(scala.util.Random.nextDouble > (1 - exp(- 100 * (goodSheep.get._1 - score) / goodSheep.get._1.toDouble))) goodSheep = Some((score, s));
			}
		}
	}
	
	override def generateWolf(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean = {
		val position = (randGen.nextInt(length), randGen.nextInt(height));
		
		if(tile(position).get.animalProcess.isEmpty) {
		    val wolf = if(!goodWolf.isEmpty && randGen.nextDouble > exp(- goodWolf.get._1 / 1600.0)) {
		    	val tmpWolf = goodWolf.get._2.generateChild(scala.util.Random, true);
		    	tmpWolf.position = position;
		    	
                tmpWolf;
		    }
		    else {
		        wolfFactory(position, randGen);
		    }

			birth(wolf, universe);
			
			true;
		}
		else false;
	}
	
	override def generateSheep(universe: Universe[T], randGen: scala.util.Random = scala.util.Random): Boolean = {
		val position = (randGen.nextInt(length), randGen.nextInt(height));
		
		if(tile(position).get.animalProcess.isEmpty) {
		    val sheep = if(!goodSheep.isEmpty && randGen.nextDouble > exp(- goodSheep.get._1 / 1600.0)) {
		    	val tmpSheep = goodSheep.get._2.generateChild(scala.util.Random, true);
		    	tmpSheep.position = position;
		    	
                tmpSheep;			        
		    }
		    else {
		        sheepFactory(position, randGen);
		    }
		    
		    birth(sheep, universe);
			
			true;
		}
		else false;
	}
	
	// Return coordinates between 0 and max
	def convertPos(pos: (Int, Int)): (Int, Int) = pos match {
		case (x, y) => ((((x + length) % length) + length) % length, (((y + height) % height) + height) % height);
	}
	
	// Return coordinates between 0 and max
	def convertPos(x: Int, y: Int): (Int, Int) = convertPos((x, y));
	
	def tile(pos: (Int, Int)): Option[Tile[T]] = {
		// Torus map
		convertPos(pos) match {
			case (x, y) => Some(tiles(x)(y));
		}
		
		// Rectangular map
//			if(x >= 0 && y >= 0 && x < length && y < height) Some(tiles(x)(y));
//			else None;
	}
	
	def tile(x: Int, y: Int): Option[Tile[T]] = tile((x, y));
	
	def addPosDir(a: (Int, Int), b: (Int, Int)): (Int, Int) = convertPos((a._1 + b._1, a._2 + b._2));
	
	def neighbourhood(pos: (Int, Int), dst: Int): List[((Int, Int), Tile[T])] = {
		// Torus map
		def aux(l: List[Int]): ((Int, Int), Tile[T]) = {
			val pos = convertPos((l.head, l.tail.head));
			
			(pos, tiles(pos._1)(pos._2));
		}
		
		utils.Maths.product(List(List.range(pos._1 - dst, pos._1 + dst), List.range(pos._2 - dst, pos._2 + dst))).map(aux);
		
		// Rectangular map
//			@tailrec
//			def aux(l: List[(Int, Int)], result: List[((Int, Int), Tile[T])]): List[((Int, Int), Tile[T])] = l match {
//				case Nil => result;
//				
//				case h::t => tile(h) match {
//					case None => aux(t, result);
//					case Some(tile) => aux(t, ((h, tile) :: result));
//				}
//			}
//			
//			aux(utils.Maths.product(List(List.range(pos._1 - dst, pos._1 + dst), List.range(pos._2 - dst, pos._2 + dst))).map(l => (l.head, l.tail.head)), Nil);
	}
	
	def print = {
		for(x <- 0 to (length - 1)) {
			for(y <- 0 to (height - 1)) {
				val symbol = tiles(x)(y).animalProcess match {
					case None => '_';
					case Some(aP) => aP.animal match {
					    case _: Wolf => 'w';
					    case _: Sheep => 's';
					    case _ => 'u';
					}
				}
				
				Console.print(symbol + " ");
			}
			
			Console.println("");
		}
	}
}

object Benchmark {
    def main(args: Array[String]): Unit = {
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
		    "benchType",
		    "nbSheeps",
		    "sheepDna",
		    "nbWolves",
		    "wolfDna",
		    "nbProc",
		    "nbIter",
		    "warmUp"
		);
		
		val arguments = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val benchType: String = Try(arguments("benchType")).toOption match {
		    case None => "universePar";
		    case Some(value) => value;
		}
		
		val nbSheeps: Int = Try(arguments("nbSheeps").toInt).toOption match {
		    case None => 0;
		    case Some(value) => value;
		}
		
		val nbWolves: Int = Try(arguments("nbWolves").toInt).toOption match {
		    case None => 0;
		    case Some(value) => value;
		}
		
		val sheepFactory: (((Int, Int), scala.util.Random) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int), scala.util.Random) => Wolf) = Main.loadWolfFactory(arguments);
		
		val nbProc: Int = Try(arguments("nbProc").toInt).toOption match {
		    case None => 4;
		    case Some(value) => value;
		}
		
		val nbIter: Int = Try(arguments("nbIter").toInt).toOption match {
		    case None => 10;
		    case Some(value) => value;
		}
		
		val warmUp: Int = Try(arguments("warmUp").toInt).toOption match {
		    case None => 10;
		    case Some(value) => value;
		}
		
		val mapLength, mapHeight = (nbSheeps + nbWolves + 1);
		
		val randGen: scala.util.Random = scala.util.Random;
		
		benchType match {
		    case "universePar" => {
                def processFactory(animal: Animal): AnimalActor = {
                    new AnimalActorImp(animal);
                }
                
		        // Generate the map
		        val map = new MapImp[AnimalActor](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalActor](), processFactory, wolfFactory, sheepFactory, 0, 0);
		        
		        val universe = new UniverseParBenchmark(map, false, nbIter, false, nbProc, warmUp);
		        
		        for(i <- 0 until nbSheeps) {
		            val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
		            
			        val sheep = sheepFactory(position, randGen);
			        map.birth(sheep, universe);
		        }
		        
		        for(i <- 0 until nbWolves) {
		            val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
		            
			        val wolf = wolfFactory(position, randGen);
			        map.birth(wolf, universe);
		        }
		        
		        universe.start;
		    }
		    
		    case "universeSeq" => {
                def processFactory(animal: Animal): AnimalSimple = {
                    new AnimalSimpleImp(animal);
                }
                
		        // Generate the map
		        val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
		        
		        val universe = new UniverseSeqBenchmark(map, false, nbIter, false, warmUp);
		        
		        for(i <- 0 until nbSheeps) {
		            val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
		            
			        val sheep = sheepFactory(position, randGen);
			        map.birth(sheep, universe);
		        }
		        
		        for(i <- 0 until nbWolves) {
		            val position = (randGen.nextInt(mapLength), randGen.nextInt(mapHeight));
		            
			        val wolf = wolfFactory(position, randGen);
			        map.birth(wolf, universe);
		        }
		        
		        universe.start;
		    }
		    
		    case _ => Console.println("Unknown benchmark type \"" + benchType + "\".");
		}
    }
}

object MemoryConsumption {
    def main(args: Array[String]): Unit = {
		val randGen: scala.util.Random = scala.util.Random;
		
		val argumentsNames: immutable.Set[String] = immutable.Set[String](
			"configFile",
		    "mapLength",
		    "mapHeight",
		    "sheepDna",
		    "wolfDna",
		    "nbSheeps",
		    "nbWolves"
		);
		
		val arguments: mutable.Map[String, String] = Main.parseArguments(argumentsNames, args);
		
		// Read the configuration file if one is given as argument
		arguments.get("configFile") match {
			case Some(fileName) => {
				arguments ++= Source.fromFile(fileName).getLines.map(_.split("\t", 2)).map(x => (x(0), x(1)));
			}
			
			case None => ();
		}
		
		val mapLength: Int = Try(arguments("mapLength").toInt).toOption match {
		    case None => 128;
		    case Some(value) => value;
		}
		
		val mapHeight: Int = Try(arguments("mapHeight").toInt).toOption match {
		    case None => 128;
		    case Some(value) => value;
		}
		
		val nbSheeps: Int = Try(arguments("nbSheeps").toInt).toOption match {
		    case None => 100;
		    case Some(value) => value;
		}
		
		val nbWolves: Int = Try(arguments("nbWolves").toInt).toOption match {
		    case None => 100;
		    case Some(value) => value;
		}
        
        val runtime = Runtime.getRuntime;
        
        val ramInit = runtime.totalMemory - runtime.freeMemory;
        Console.println("** Used Memory:  " + ramInit);
		
		def processFactory(animal: Animal): AnimalSimple = {
		    new AnimalSimpleImp(animal);
		}
		
		val sheepFactory: (((Int, Int), scala.util.Random) => Sheep) = Main.loadSheepFactory(arguments);
		
		val wolfFactory: (((Int, Int), scala.util.Random) => Wolf) = Main.loadWolfFactory(arguments);
        
		val map = new MapImp[AnimalSimple](mapLength, mapHeight, Array.fill(mapLength)(Array.fill(mapHeight)(new Tile(None, new Grass(randGen.nextInt(Grass.maxHeight), Grass.maxHeight, 0)))), new mutable.HashSet[AnimalSimple](), processFactory, wolfFactory, sheepFactory, 0, 0);
		
		for(i <- 0 to nbSheeps) {
			val sheep = sheepFactory((0, 0), randGen);
	        val animalProcess = processFactory(sheep);
		    map.animalProcesses += animalProcess;
		}
		
		for(i <- 0 to nbWolves) {
			val wolf = wolfFactory((0, 0), randGen);
	        val animalProcess = processFactory(wolf);
		    map.animalProcesses += animalProcess;
		}
		
		val ramEnd = runtime.totalMemory - runtime.freeMemory;
        Console.println("** Used Memory: " + ramEnd);
        Console.println("\t" + (ramEnd - ramInit));
    }
}
