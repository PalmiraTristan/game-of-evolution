// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package animal;

import scala.annotation.tailrec;

import scala.actors.Actor;
import scala.math.max;
import scala.math.min;

object Direction extends Enumeration {
	val N, S, E, W = Value;
	
	def convert(value: Int): Direction.Value = value match {
		case 0 => N;
		case 1 => E;
		case 2 => S;
		case _ => W;
	}
	
	def convert(direction: Direction.Value): (Int, Int) = direction match {
		case N => (-1, 0);
		case S => (1, 0);
		case E => (0, 1);
		case W => (0, -1);
	}
}

trait Dna {
	def interact(perceptionVec: Array[Double]): Array[Double];
	def mutate(randGen: scala.util.Random): Dna;
	
	def description: String;
}

trait DnaNN extends Dna {
    def layers: Seq[NNLayer];
    
    def interact(perceptionVec: Array[Double]): Array[Double] = {
        layers.foldLeft(perceptionVec)((vec, layer) => layer.process(vec));
    }
    
    def description: String = "DnaNN(" + layers.map(_.description).mkString(", ") + ")";
}

class DnaNNImp(val layers: Seq[NNLayer]) extends DnaNN {
    def mutate(randGen: scala.util.Random): DnaNNImp = {
        new DnaNNImp(layers.map(_.mutate(randGen)));
    }
}

trait DnaRNN extends Dna {
	def initLayers: Seq[NNLayer];
	def mainLayers: Seq[NNLayer];
	def finalLayers: Seq[NNLayer];
	
	def currentMemory: Array[Double];
	def currentMemory_=(vec: Array[Double]): Unit;
    
    def interact(perceptionVec: Array[Double]): Array[Double] = {
        val v = initLayers.foldLeft(perceptionVec)((vec, layer) => layer.process(vec));
        
        currentMemory = mainLayers.foldLeft(v ++ currentMemory)((vec, layer) => layer.process(vec));
        
        finalLayers.foldLeft(currentMemory)((vec, layer) => layer.process(vec));
    }
    
    def description: String = "DnaRNN(" + initLayers.map(_.description).mkString(", ") + "; "  + mainLayers.map(_.description).mkString(", ") + "; "  + finalLayers.map(_.description).mkString(", ") + ")";
}

class DnaRNNImp(val initLayers: Seq[NNLayer], val mainLayers: Seq[NNLayer], val finalLayers: Seq[NNLayer], var currentMemory: Array[Double]) extends DnaRNN {
    def mutate(randGen: scala.util.Random): DnaRNNImp = {
        new DnaRNNImp(initLayers.map(_.mutate(randGen)), mainLayers.map(_.mutate(randGen)), finalLayers.map(_.mutate(randGen)), currentMemory.map(_ => 0.0));
    }
}

trait NNLayer {
	def process(vec: Array[Double]): Array[Double];
	def mutate(randGen: scala.util.Random): NNLayer;
	
	def description: String;
}

object NNLayerMatrix {
	def apply(randGen: scala.util.Random, numberActions: Int, vecSize: Int, f: (Array[Double] => Array[Double]), mutationFactor: Double): NNLayerMatrix = {
		val matrixLayer = Array.ofDim[Double](numberActions, vecSize);
		
		for(i <- 0 to (numberActions - 1)) {
			for(j <- 0 to (vecSize - 1)) {
				matrixLayer(i)(j) = randGen.nextDouble * 2 - 1;
			}
		}
		
		new NNLayerMatrix(matrixLayer, f, mutationFactor);
	}
}

class NNLayerMatrix(val data: Array[Array[Double]], val f: (Array[Double] => Array[Double]), val mutationFactor: Double) extends NNLayer {
	def process(vec: Array[Double]): Array[Double] = f(utils.Maths.multiplySeq(data, vec));
	
	def mutate(randGen: scala.util.Random): NNLayerMatrix = new NNLayerMatrix(data.map(x => x.map(y => y + mutationFactor * (randGen.nextDouble - 0.5))), f, mutationFactor);
	
	override def toString: String = {
	    val formatter = new java.text.DecimalFormat("#.###");
	    
	    data.map(line => line.map(x => formatter.format(x)).mkString(" \t")).mkString("\n");
	}
	
	def description: String = "NNLayerMatrix(" + data.length + ", " + data(0).length + ")";
}

object NNLayer3DTensor {
	def apply(randGen: scala.util.Random, numberActions: Int, vecSize: Int, f: (Array[Double] => Array[Double]), mutationFactor: Double): NNLayer3DTensor = {
		val tensorLayer = Array.ofDim[Double](numberActions, vecSize, vecSize);
		
		for(i <- 0 to (numberActions - 1)) {
			for(j <- 0 to (vecSize - 1)) {
				for(k <- 0 to (vecSize - 1)) {
					tensorLayer(i)(j)(k) = randGen.nextDouble * 2 - 1;
				}
			}
		}
		
		new NNLayer3DTensor(tensorLayer, f, mutationFactor);
	}
}

class NNLayer3DTensor(val data: Array[Array[Array[Double]]], val f: (Array[Double] => Array[Double]), val mutationFactor: Double) extends NNLayer {
	def process(vec: Array[Double]): Array[Double] = f(utils.Maths.multiplySeqSeq(data, vec));
	
	def mutate(randGen: scala.util.Random): NNLayer3DTensor = new NNLayer3DTensor(data.map(x => x.map(y => y.map(z => z + mutationFactor * (randGen.nextDouble - 0.5)))), f, mutationFactor);
	
	override def toString: String = {
	    val formatter = new java.text.DecimalFormat("#.###");
	    
	    data.map(matrix => matrix.map(line => line.map(x => formatter.format(x)).mkString(" \t")).mkString("\n")).mkString("\n- - - - - -\n");
	}
	
	def description: String = "NNLayer3DTensor(" + data.length + ", " + data(0).length + ", " + data(0)(0).length + ")";
}

trait CauseOfDeath;

case object Starvation extends CauseOfDeath;
case object OldAge extends CauseOfDeath;
case class Killed(killer: String) extends CauseOfDeath;

abstract class AnimalTracker {
    def onAct(action: Action): Unit;
    def onDie(cause: CauseOfDeath): Unit;
    def track(message: String): Unit;
}

object AnimalTrackerImpl extends AnimalTracker {
    def onAct(action: Action): Unit = Console.println(action.toString);
    def onDie(cause: CauseOfDeath): Unit = Console.println("death from " + cause + "\n");
    def track(message: String): Unit = Console.println(message);
}

trait AnimalProcess {
    def animal: Animal;
    def isAlive: Boolean = {
        animal.alive;
    }
    
	def getOlder[T >: this.type <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
	    animal.getOlder match {
	        case None => ();
	        case Some(cause) => die(cause, map, universe);
	    }
	}
	
	def die[T >: this.type <: AnimalProcess](cause: CauseOfDeath, map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
	    map.tile(animal.position).get.animalProcess = None;
	    animal.die(cause);
	    map.death(this, cause, universe);
	}
	
	def performAction[T >: this.type <: AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
	    animal.performAction(action, time, map, universe) match { // This has to be done here because the map needs the AnimalProcess and not just the Animal
            case Some(newPosition: (Int, Int)) => {
			    map.tile(animal.position).get.animalProcess = None;
			    
			    animal.position = newPosition;
			    map.tile(newPosition).get.animalProcess = Some(this): Option[this.type];
            }
            
            case _ => ();
        }
	}
	
//        override def equals(o: Any) = super.equals(o)
//        override def hashCode = super.hashCode
}

trait AnimalSimple extends AnimalProcess {
    def act(time: Int, map: evolution.Map[AnimalSimple], universe: evolution.Universe[AnimalSimple]): Unit = {
        val action = animal.chooseAction(time, map);
        
        performAction(action, time, map, universe);
    }
}

class AnimalSimpleImp(val animal: Animal) extends AnimalSimple;

trait AnimalActor extends AnimalProcess with Actor {
    def act() = {
	    loop {
	        react {
	            case evolution.ChooseAction(time, map) => {
	                sender ! evolution.Choice(this, animal.chooseAction(time, map));
	            }
	            
	            case evolution.Stop => exit();
	        }
	    }
	}
}

class AnimalActorImp(val animal: Animal) extends AnimalActor;

abstract class Animal {
    var alive: Boolean;

	def chooseAction[T<:AnimalProcess](time: Int, map: evolution.Map[T]): Action;
    def performAction[T<:AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)];
    
	def getOlder: Option[CauseOfDeath];
	
	def die(cause: CauseOfDeath): Unit = {
		alive = false;
		
		if(!tracker.isEmpty) tracker.get.onDie(cause);
	}
	
	var tracker: Option[AnimalTracker];
	
	def dna: Dna;
	
	def age: Int;
	
	def position: (Int, Int);
	def position_=(pos: (Int, Int)): Unit;
	
	def healthPoints: Int;
	def healthPoints_=(hitPoints: Int): Unit;
	
	val generation: Int;
    def ageLineage: Int;
}

object Species {
    def apply(animal: Animal): AnimalSpecies = animal match {
        case s: Sheep => SheepType;
        case w: Wolf => WolfType;
    }
}

trait AnimalSpecies;

case object WolfType extends AnimalSpecies {
	override def toString = "WolfType";
}

case object SheepType extends AnimalSpecies {
	override def toString = "SheepType";
}

trait Action;

trait ActionWolf extends Action;

object WolfMove {
	def apply(direction: Direction.Value): WolfMove = {
		new WolfMove(direction);
	} 
}

class WolfMove(val direction: Direction.Value) extends ActionWolf {
	override val toString: String = "WolfMove(" + direction + ")";
}

object WolfNothing extends ActionWolf {
	override val toString: String = "WolfNothing";
}

object WolfProcreate extends ActionWolf {
	override val toString: String = "WolfProcreate";
}

trait Wolf extends Animal {
	def age: Int;
	def food: Int;
	
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): Wolf;
}

class WolfBasicReference (
	val lifeExpectancy: Int, // 200
	val energyLimit: Int, // 50
	val hungerRate: Int, // 1
	val sightRange: Int, // 2
	val maxHealthPoints: Int, // 100
	val strength: Int // 100
) {
	val perceptionVecSize: Int = WolfBasic.infoPerTile * (2 * sightRange + 1) * (2 * sightRange + 1) + 2;
}

object WolfBasic {
	val infoPerTile = 4;
	val numberActions = 6; // 4 moves, nothing, procreation
}

class WolfImpBasic(
	val reference: WolfBasicReference,
	var alive: Boolean,
	var tracker: Option[AnimalTracker],
	var age: Int,
	var energy: Int,
	var healthPoints: Int,
	var position: (Int, Int),
	val dna: Dna,
	val generation: Int,
	val ageLineage: Int
) extends Wolf {
	def this(reference: WolfBasicReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	def food: Int = energy;
	
	def performAction[T<:AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)] = {
	    if(!tracker.isEmpty) {
	    	tracker.get.track("energy=" + energy + "; age=" + age);
	    	tracker.get.onAct(action);
	   	}
	    
	    action match {
		    case move: WolfMove => {
			    val targetPosition = map.addPosDir(Direction.convert(move.direction), position);
			
			    map.tile(targetPosition) match {
				    case None => {
				        if(!tracker.isEmpty) tracker.get.track("that's the edge of the world");
				        
				        None;
				    }
				
				    case Some(targetTile) => targetTile.animalProcess match {
					    case None => {
					        Some(targetPosition);
					    }
					
					    case Some(animalProcess) => {
					    	animalProcess.animal.healthPoints -= reference.strength;
					    	
					    	
						    if(!tracker.isEmpty) tracker.get.track("attacked " + animalProcess.animal);
					    	
					    	if(animalProcess.animal.healthPoints <= 0) {
								val food = animalProcess.animal match {
									case wolf: Wolf => {
										// TODO
										//val food = wolf.food / 2;
										val food = 2;
										
										if(!tracker.isEmpty) tracker.get.track("ate " + food + " food");
										
										food;
									}
						
									case sheep: Sheep => {
										val food = sheep.food;
									
										if(!tracker.isEmpty) tracker.get.track("ate " + food + " food");
										
										food;
									}
								}
								
								energy = min(reference.energyLimit, energy + food);
						
								animalProcess.die(Killed("wolf"), map, universe);
								
								Some(targetPosition);
						    }
						    else {
						    	None;
						    }
					    }
				    }
			    }
		    }
		
		    case WolfProcreate => {
		    	procreate(map, universe);
			    
			    None;
		    }
		
		    case WolfNothing => None; // Nothing LOL
		
		    case other => None; //Console.println("Not codded yet (" + other + ")");
	    }
	}
	
	def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		map.neighbourhood(position, 1).find(_._2.animalProcess.isEmpty) match {
		    case None => if(!tracker.isEmpty) tracker.get.track("no free tile to give birth"); // There is no free tile
		
		    case Some((babyPos, _)) => {
				val food = energy / 2 - 1;
		
				energy = food;
		
				if(food > 0) {
					val child = generateChild(scala.util.Random);
					child.energy = food;
					child.position = babyPos;
					
					map.birth(child, universe);
				}
				else if(!tracker.isEmpty) tracker.get.track("not enough food to procreate");
		    }
	    }
	}

	// Get the perception vector
	// TODO See the health points and the age of the different animals
	def perception[T<:AnimalProcess](time: Int, map: evolution.Map[T]): Array[Double] = {
		val sightRange = 2;
		val infoPerTile = 4;
		
		// The perception vector
		val perceptionVec = new Array[Double](reference.perceptionVecSize);
		
		for(dx <- Range(- reference.sightRange, reference.sightRange + 1)) {
			for(dy <- Range(- reference.sightRange, reference.sightRange + 1)) {
				// Position of the tile we're looking at
				val px = position._1 + dx;
				val py = position._2 + dy;
				
				// Beginning of the corresponding subvector in perceptionVec
				val refId = ((dx + reference.sightRange) * (2 * reference.sightRange + 1) + (dy + reference.sightRange)) * WolfBasic.infoPerTile;
	
				map.tile(px, py) match {
					case Some(tile) => {
						perceptionVec(refId + 0) = 1; // Is there a tile here (it could be the edge of the world)
			
						tile.animalProcess match {
						    case Some(aP) => aP.animal match {
							    case _: Wolf => {
								    perceptionVec(refId + 1) = 1; // Is there a wolf here
							    }
						
							    case _: Sheep => {
								    perceptionVec(refId + 2) = 1; // Is there a sheep here
							    }
							}
							
							case _ => ();
						}
		
						perceptionVec(refId + 3) = tile.updateGrass(time).toDouble / evolution.Grass.maxHeight; // What amout of grass is there
					}
					
					case None => ();
				}
			}
		}
		
		// Energy
		perceptionVec(perceptionVec.size - 2) = (energy.toDouble / reference.energyLimit);
		
		// Constant
		perceptionVec(perceptionVec.size - 1) = 1.0;
		
		perceptionVec;
	}
	
	// Choose an action
	def chooseAction[T<:AnimalProcess](time: Int, map: evolution.Map[T]): ActionWolf = {
		val perceptionVec = perception(time, map);
		
		val actionId = utils.Maths.randomPick(dna.interact(perceptionVec));
		
		if(actionId < 4) WolfMove(Direction.convert(actionId));
		else if(actionId == 4) WolfNothing;
		else WolfProcreate;
	}
	
	// Generate a child
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): WolfImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new WolfImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new WolfImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
	
	def getOlder: Option[CauseOfDeath] = {
		age += 1;
		energy -= reference.hungerRate;
		
		if(age > reference.lifeExpectancy) Some(OldAge);
		else if(energy <= 0) Some(Starvation);
		else None;
	}
}

class WolfGrowUpReference (
	lifeExpectancy: Int, // 200
	energyLimit: Int, // 50
	hungerRate: Int, // 1
	sightRange: Int, // 2
	maxHealthPoints: Int, // 100
	strength: Int, // 100,
	val adulthood: (Int, Int) // (20, 160)
) extends WolfBasicReference(lifeExpectancy, energyLimit, hungerRate, sightRange, maxHealthPoints, strength);

// Very similar to WolfImpBasic, the only difference is they can have babies only during a certain period of time
class WolfImpGrowUp(
	reference: WolfGrowUpReference,
	alive0: Boolean, // Scala makes constructor parameters directly available to the methods in the class, so if I want to use a mutable parameter in one of this class's methods, I need to change the name of this parameter
	tracker0: Option[AnimalTracker], 
	age0: Int,
	energy0: Int,
	healthPoints0: Int,
	position0: (Int, Int),
	dna: Dna,
	generation: Int,
	ageLineage: Int
) extends WolfImpBasic(reference, alive0, tracker0, age0, energy0, healthPoints0, position0, dna, generation, ageLineage) {
	def this(reference: WolfGrowUpReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	// Is the wolf adult?
	def isAdult: Boolean = (age >= reference.adulthood._1) && (age < reference.adulthood._2);
	
	override def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		if(!isAdult) {
			if(!tracker.isEmpty) tracker.get.track("not in age of procreation");
			return;
		}
		
		super.procreate(map, universe);
	}
	
	// Generate a child
	override def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): WolfImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new WolfImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new WolfImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
}

trait ActionSheep extends Action;

object SheepMove {
	def apply(direction: Direction.Value): SheepMove = {
		new SheepMove(direction);
	} 
}

class SheepMove(val direction: Direction.Value) extends ActionSheep {
	override val toString: String = "SheepMove(" + direction + ")";
}

object SheepEat extends ActionSheep {
	override val toString: String = "SheepEat";
}

object SheepProcreate extends ActionSheep {
	override val toString: String = "SheepProcreate";
}

trait Sheep extends Animal {
	def age: Int;
	def food: Int;
	
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): Sheep;
}

class SheepBasicReference(
	val lifeExpectancy: Int, // 100
	val energyLimit: Int, // 60
	val hungerRate: Int, // 4
	val sightRange: Int, // 2
	val maxHealthPoints: Int, // 100
	val strength: Int // 35
) {
	val perceptionVecSize = SheepBasic.infoPerTile * (2 * sightRange + 1) * (2 * sightRange + 1) + 2;
}

object SheepBasic {
	val infoPerTile = 4;
	val numberActions = 6; // 4 moves, eat, procreation
}

class SheepImpBasic(
	val reference: SheepBasicReference,
	var alive: Boolean,
	var tracker: Option[AnimalTracker],
	var age: Int,
	var energy: Int,
	var healthPoints: Int,
	var position: (Int, Int),
	val dna: Dna,
	val generation: Int,
	val ageLineage: Int
) extends Sheep {
	def this(reference: SheepBasicReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	def food: Int = energy;
	
	def performAction[T<:AnimalProcess](action: Action, time: Int, map: evolution.Map[T], universe: evolution.Universe[T]): Option[(Int, Int)] = {
	    if(!tracker.isEmpty) {
	    	tracker.get.track("energy=" + energy + "; age=" + age);
	    	tracker.get.onAct(action);
	   	}
	    
	    action match {
		    case move: SheepMove => {
			    val targetPosition = map.addPosDir(Direction.convert(move.direction), position);

			    map.tile(targetPosition) match {
				    case None => {
				        if(!tracker.isEmpty) tracker.get.track("that's the edge of the world");
				        
				        None;
				    }
			
				    case Some(targetTile) => targetTile.animalProcess match {
					    case None => {
					        Some(targetPosition);
					    }
				
					    case Some(animalProcess) => {
					    	animalProcess.animal.healthPoints -= reference.strength;
					    	
					        if(!tracker.isEmpty) tracker.get.track("attacked " + animalProcess.animal);
					        
					        if(animalProcess.animal.healthPoints <= 0) {
								animalProcess.die(Killed("wolf"), map, universe);
								
								Some(targetPosition);
							}
							else {
						        None;
						    }
					    }
				    }
			    }
		    }
		
		    case SheepProcreate => {
				procreate(map, universe);
			    
			    None;
		    }
		
		    case SheepEat => {
			    val grassQty = min(reference.energyLimit - energy, map.tile(position).get.updateGrass(time));
			
			    map.tile(position).get.updateGrass(time, -grassQty);
			    energy += grassQty;
			    
			    if(!tracker.isEmpty) tracker.get.track("ate " + grassQty + " unit(s) of grass");
			    
			    None;
		    }
		
		    case other => None;//Console.println("Not codded yet (" + other + ")");
	    }
	}
	
	def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		map.neighbourhood(position, 1).find(_._2.animalProcess.isEmpty) match {
		    case None => if(!tracker.isEmpty) tracker.get.track("not free tile to give birth"); // There is no free tile
		
		    case Some((babyPos, _)) => {
				val food = energy / 2 - 1;
		
				energy = food;
		
				if(food > 0) {
					val child = generateChild(scala.util.Random);
					child.energy = food;
					child.position = babyPos;
					
					map.birth(child, universe);
				}
				else if(!tracker.isEmpty) tracker.get.track("not enough food to procreate");
		    }
	    }
	}
	
	// Get the perception vector
	def perception[T<:AnimalProcess](time: Int, map: evolution.Map[T]): Array[Double] = {
		val sightRange = 2;
		val infoPerTile = 4;
		
		// The perception vector
		val perceptionVec = new Array[Double](reference.perceptionVecSize);
		
		for(dx <- Range(-reference.sightRange, reference.sightRange + 1)) {
			for(dy <- Range(-reference.sightRange, reference.sightRange + 1)) {
				// Position of the tile we're looking at
				val px = position._1 + dx;
				val py = position._2 + dy;
				
				// Beginning of the corresponding subvector in perceptionVec
				val refId = ((dx + reference.sightRange) * (2 * reference.sightRange + 1) + (dy + reference.sightRange)) * SheepBasic.infoPerTile;
	
				map.tile(px, py) match {
					case Some(tile) => {
						perceptionVec(refId + 0) = 1; // Is there a tile here (it could be the edge of the world)
			
						tile.animalProcess match {
						    case Some(aP) => aP.animal match {
							    case _: Wolf => {
								    perceptionVec(refId + 1) = 1; // Is there a wolf here
							    }
						
							    case _: Sheep => {
								    perceptionVec(refId + 2) = 1; // Is there a sheep here
							    }
							}
							
							case _ => ();
						}
		
						perceptionVec(refId + 3) = tile.updateGrass(time).toDouble / evolution.Grass.maxHeight; // What amout of grass is there
					}
					
					case None => ();
				}
			}
		}
		
		// Energy
		perceptionVec(perceptionVec.size - 2) = (energy.toDouble / reference.energyLimit);
		
		// Constant
		perceptionVec(perceptionVec.size - 1) = 1.0;
		
		perceptionVec;
	}
	
	// Choose an action
	def chooseAction[T<:AnimalProcess](time: Int, map: evolution.Map[T]): ActionSheep = {
		val perceptionVec = perception(time, map);
		
		val actionId = utils.Maths.randomPick(dna.interact(perceptionVec));
		
		if(actionId < 4) SheepMove(Direction.convert(actionId));
		else if(actionId == 4) SheepEat;
		else SheepProcreate;
	}
	
	// Generate a child
	def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): SheepImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new SheepImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new SheepImpBasic(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
	
	def getOlder: Option[CauseOfDeath] = {
		age += 1;
		energy -= reference.hungerRate;
		
		if(age > reference.lifeExpectancy) Some(OldAge);
		else if(energy <= 0) Some(Starvation);
		else None;
	}
}

class SheepGrowUpReference(
	lifeExpectancy: Int, // 100
	energyLimit: Int, // 60
	hungerRate: Int, // 4
	sightRange: Int, // 2
	maxHealthPoints: Int, // 100
	strength: Int, // 35
	val adulthood: (Int, Int) // (10, 80)
) extends SheepBasicReference(lifeExpectancy, energyLimit, hungerRate, sightRange, maxHealthPoints, strength);

// Very similar to SheepImpBasic, the only difference is they can have babies only during a certain period of time
class SheepImpGrowUp(
	reference: SheepGrowUpReference,
	alive0: Boolean,
	tracker0: Option[AnimalTracker],
	age0: Int,
	energy0: Int,
	healthPoints0: Int,
	position0: (Int, Int),
	dna: Dna,
	generation: Int,
	ageLineage: Int
) extends SheepImpBasic(reference, alive0, tracker0, age0, energy0, healthPoints0, position0, dna, generation, ageLineage) {
	def this(reference: SheepGrowUpReference, position: (Int, Int), dna: Dna, generation: Int, ageLineage: Int) = this(reference, true, None, 0, reference.energyLimit, reference.maxHealthPoints, position, dna, generation, ageLineage);
	
	// Is the sheep adult?
	def isAdult: Boolean = (age >= reference.adulthood._1) && (age < reference.adulthood._2);
	
	override def procreate[T <: AnimalProcess](map: evolution.Map[T], universe: evolution.Universe[T]): Unit = {
		if(!isAdult) {
			if(!tracker.isEmpty) tracker.get.track("not in age of procreation");
			return;
		}
		
		super.procreate(map, universe);
	}
	
	// Generate a child
	override def generateChild(randGen: scala.util.Random, resetLineage: Boolean = false): SheepImpBasic = {
		val childDna = dna.mutate(randGen);
		
		resetLineage match {
			case false => new SheepImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, (generation + 1), (ageLineage + age));
			case true => new SheepImpGrowUp(reference, true, None, 0, 0, reference.maxHealthPoints, (0, 0), childDna, 0, 0);
		}
	}
}
